public class MultipleWrites {
	private static final String fileName = "aaa.txt";
	private static final int SIZE = 100000000;

	private static void measureTime(Runnable f) {}

	private static void write() {}

	private static void writeUsingBuffer() {}

	public static void main(String[] args) {}
}
