#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>

void measure_time(void function()) {
    static struct timeval t1, t2, t;
    gettimeofday(&t1, NULL);
    function();
    gettimeofday(&t2, NULL);
    timersub(&t2, &t1, &t);
    printf("Temps total: %ld.%06ld\n", (long)t.tv_sec, (long)t.tv_usec); 
}

void make_one_big_allocation() {
    int *p, size = 100000000;
    p = malloc(size * sizeof(int));
    free(p);
}

int main (void) {
    measure_time(make_one_big_allocation);
    return 0;
}
